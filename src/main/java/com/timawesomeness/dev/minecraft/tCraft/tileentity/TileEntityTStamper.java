/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.tileentity;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;

import com.timawesomeness.dev.minecraft.tCraft.StampingRecipes;
import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;

import cpw.mods.fml.common.network.PacketDispatcher;

public class TileEntityTStamper extends TileEntity implements ISidedInventory {

	private String localizedName;

	private static final int[] slots_top = new int[] { 0 };
	private static final int[] slots_bottom = new int[] { 1 };
	private static final int[] slots_sides = new int[] { 0 };

	private ItemStack[] slots = new ItemStack[2];

	public int stampingSpeed = 60;
	public int stampTime;

	public int getSizeInventory() {
		return this.slots.length;
	}

	public void setGuiDisplayName(String par1DisplayName) {
		this.localizedName = par1DisplayName;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return slots[i];
	}

	@Override
	public ItemStack decrStackSize(int i, int j) {
		if (slots[i] != null) {
			ItemStack itemstack;

			if (slots[i].stackSize <= j) {
				itemstack = slots[i];
				slots[i] = null;
				return itemstack;
			} else {
				itemstack = slots[i].splitStack(j);

				if (slots[i].stackSize == 0) {
					slots[i] = null;
				}

				return itemstack;
			}
		}

		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		if (slots[i] != null) {
			ItemStack itemstack = slots[i];
			slots[i] = null;
			return itemstack;
		}

		return null;
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack) {
		slots[i] = itemstack;

		if (itemstack != null && itemstack.stackSize > getInventoryStackLimit()) {
			itemstack.stackSize = getInventoryStackLimit();
		}
	}

	@Override
	public String getInvName() {
		return TCraftMain.tStamper.getLocalizedName();
	}

	@Override
	public boolean isInvNameLocalized() {
		return true;
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this ? false : entityplayer.getDistanceSq((double) xCoord + 0.5D, (double) yCoord + 0.5D, (double) zCoord + 0.5D) <= 64D;
	}

	@Override
	public void readFromNBT(NBTTagCompound par1Tag) {
		super.readFromNBT(par1Tag);

		NBTTagList tagList = par1Tag.getTagList("Items");
		slots = new ItemStack[getSizeInventory()];

		for (int i = 0; i < tagList.tagCount(); i++) {
			NBTTagCompound tagCompound = (NBTTagCompound) tagList.tagAt(i);
			byte j = tagCompound.getByte("Slot");

			if (j >= 0 && j < slots.length) {
				slots[j] = ItemStack.loadItemStackFromNBT(tagCompound);
			}
		}

		stampTime = par1Tag.getShort("stampTime");

		if (par1Tag.hasKey("ChangedName")) {
			localizedName = par1Tag.getString("ChangedName");
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound par1Tag) {
		super.writeToNBT(par1Tag);

		par1Tag.setShort("stampTime", (short) stampTime);

		NBTTagList tagList = new NBTTagList();
		for (int i = 0; i < slots.length; i++) {
			if (slots[i] != null) {
				NBTTagCompound tagCompound = new NBTTagCompound();
				tagCompound.setByte("Slot", (byte) i);
				slots[i].writeToNBT(tagCompound);
				tagList.appendTag(tagCompound);
			}
		}

		par1Tag.setTag("Items", tagList);

		if (isInvNameLocalized() && localizedName != null) {
			par1Tag.setString("ChangedName", localizedName);
		}
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	@Override
	public void updateEntity() {
		boolean flag = false;

		if (!worldObj.isRemote) {
			if (canStamp()) {
				stampTime++;

				if (stampTime == stampingSpeed) {
					stampTime = 0;
					stampItem();
					flag = true;
				}

			} else {
				stampTime = 0;
			}

			if (flag) {
				onInventoryChanged();
			}
		}
	}

	private boolean canStamp() {
		if (slots[0] == null) {
			return false;
		} else {
			ItemStack itemstack = StampingRecipes.getStampingResult(slots[0].itemID);

			if (itemstack == null)
				return false;
			if (slots[1] == null)
				return true;
			if (!slots[1].isItemEqual(itemstack))
				return false;

			int result = slots[1].stackSize + itemstack.stackSize;

			return (result <= getInventoryStackLimit() && result <= itemstack.getMaxStackSize());
		}
	}

	public void stampItem() {
		if (canStamp()) {
			ItemStack itemstack = StampingRecipes.getStampingResult(slots[0].itemID);

			if (slots[1] == null) {
				slots[1] = itemstack.copy();
			} else if (slots[1].isItemEqual(itemstack)) {
				slots[1].stackSize += itemstack.stackSize;
			}

			slots[0].stackSize--;

			PacketDispatcher.sendPacketToAllPlayers(getParticlePacket(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5));

			if (slots[0].stackSize <= 0) {
				slots[0] = null;
			}
		}
	}

	public Packet250CustomPayload getParticlePacket(double x, double y, double z) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(bos);

		try {
			dos.writeDouble(x);
			dos.writeDouble(y);
			dos.writeDouble(z);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new Packet250CustomPayload("0", bos.toByteArray());
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return i == 1 ? false : true;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
		return var1 == 0 ? slots_bottom : (var1 == 1 ? slots_top : slots_sides);
	}

	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j) {
		return isItemValidForSlot(i, itemstack);
	}

	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j) {
		return i != 0;
	}

	public int getStampProgressScaled(int i) {
		return stampTime * i / stampingSpeed;
	}

}
