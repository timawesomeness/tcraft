/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft;

import net.minecraft.client.Minecraft;

public class CommonProxy {
	public void registerRenderers() {
		//Nothing goes here.
	}
	
	public void printMessageToPlayer(String msg) {
		Minecraft.getMinecraft().thePlayer.addChatMessage(msg);
	}
	
}
