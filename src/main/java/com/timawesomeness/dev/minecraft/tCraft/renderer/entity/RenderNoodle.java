/*
 * Copyright (C) 2014 timawesomeness.
 * Use of this source code is governed by the timawesomeness Open Source License v1.4.
 */
package com.timawesomeness.dev.minecraft.tCraft.renderer.entity;

import com.timawesomeness.dev.minecraft.tCraft.ModInfo;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderNoodle extends RenderLiving {
	
	private static final ResourceLocation texture = new ResourceLocation(ModInfo.modid + ":textures/entity/noodle.png");
	
	public RenderNoodle(ModelBase par1ModelBase, float par2) {
		super(par1ModelBase, par2);
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity) {
		return texture;
	}

}
