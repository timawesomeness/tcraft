/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */
package com.timawesomeness.dev.minecraft.tCraft;

import cpw.mods.fml.common.registry.LanguageRegistry;

public class LanguageRegistration {
	
	public static void registerLangThings() {
		LanguageRegistry.instance().addStringLocalization("itemGroup.tabTCraft", "en_US", "tCraft");
		LanguageRegistry.addName(TCraftMain.tOre, "t Ore");
		LanguageRegistry.addName(TCraftMain.tBlock, "t Block");
		LanguageRegistry.addName(TCraftMain.tDirt, "t Dirt");
		LanguageRegistry.addName(TCraftMain.tIngot, "t Ingot");
		LanguageRegistry.addName(TCraftMain.tIngotBranded, "Branded t Ingot (NYI)");
		LanguageRegistry.addName(TCraftMain.tPickaxe, "t Pickaxe");
		LanguageRegistry.addName(TCraftMain.tSword, "t Sword");
		LanguageRegistry.addName(TCraftMain.tShovel, "t Shovel");
		LanguageRegistry.addName(TCraftMain.tAxe, "t Axe");
		LanguageRegistry.addName(TCraftMain.tHoe, "t Hoe");
		LanguageRegistry.addName(TCraftMain.tPortal, "t Portal");
		LanguageRegistry.addName(TCraftMain.tStone, "t Stone");
		LanguageRegistry.addName(TCraftMain.tSapling, "t Sapling");
		LanguageRegistry.addName(TCraftMain.tHelmet, "t Helmet");
		LanguageRegistry.addName(TCraftMain.tChestplate, "t Chestplate");
		LanguageRegistry.addName(TCraftMain.tLeggings, "t Leggings");
		LanguageRegistry.addName(TCraftMain.tBoots, "t Boots");
		LanguageRegistry.addName(TCraftMain.tCobble, "t Cobblestone");
		LanguageRegistry.addName(TCraftMain.tBacon, "Bacon");
		LanguageRegistry.addName(TCraftMain.tStamper, "t Stamper");
		LanguageRegistry.addName(TCraftMain.tStamp, "t Stamp");
		LanguageRegistry.instance().addStringLocalization("entity.Noodle.name", "en_US", "Noodle");
		LanguageRegistry.addName(TCraftMain.tNoodleFood, "Bacon Noodles");
	}
	
}
