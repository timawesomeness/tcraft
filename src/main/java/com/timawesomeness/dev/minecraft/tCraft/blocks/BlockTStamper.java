/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.blocks;

import java.util.Random;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

import com.timawesomeness.dev.minecraft.tCraft.ModInfo;
import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;
import com.timawesomeness.dev.minecraft.tCraft.tileentity.TileEntityTStamper;

import cpw.mods.fml.common.network.FMLNetworkHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockTStamper extends BlockContainer {
	
	private Random random = new Random();
	
	public BlockTStamper(int par1, Material par2Material) {
		super(par1, par2Material);
		setCreativeTab(TCraftMain.tabTCraft);
		setHardness(3.5F);
		
	}
	
	@SideOnly(Side.CLIENT)
	public static Icon sideIcon;
	public static Icon tbIcon;
	
	@Override
	@SideOnly(Side.CLIENT) 
    public void registerIcons(IconRegister par1IconRegister)
    {
        sideIcon = par1IconRegister.registerIcon(ModInfo.modid + ":" + this.getUnlocalizedName().substring(5) + "_side");
        tbIcon = par1IconRegister.registerIcon(ModInfo.modid + ":" + this.getUnlocalizedName().substring(5));
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIcon(int side, int metadata) {
		if(side == 0 || side == 1) {
			return tbIcon;
		} else {
			return sideIcon;
		}
	}
	
	public TileEntity createNewTileEntity(World par1World) {
		return new TileEntityTStamper();
	}
	
	public void onBlockPlacedBy(World par1World, int par2X, int par3Y, int par4Z, EntityLivingBase par5EntityLivingBase, ItemStack par6ItemStack) {
		if (par6ItemStack.hasDisplayName()) {
			((TileEntityTStamper)par1World.getBlockTileEntity(par2X, par3Y, par4Z)).setGuiDisplayName(par6ItemStack.getDisplayName());
		}
	}
	
	@Override
	public boolean onBlockActivated(World par1World, int par2X, int par3Y, int par4Z, EntityPlayer par5EntityPlayer, int par6Side, float par7HitX, float par8HitY, float par9HitZ) {
		if (!par1World.isRemote) {
			FMLNetworkHandler.openGui(par5EntityPlayer, TCraftMain.instance, TCraftMain.tStamperGuiID, par1World, par2X, par3Y, par4Z);
		}
		return true;
	}
	
	@Override
	public boolean hasComparatorInputOverride() {
		return true;
	}
	
	@Override
	public int getComparatorInputOverride(World par1World, int x, int y, int z, int i) {
		return Container.calcRedstoneFromInventory((IInventory)par1World.getBlockTileEntity(x, y, z));
	}
	
	@Override
	public void breakBlock(World par1World, int x, int y, int z, int par5, int par6) {	
		TileEntityTStamper tileEntity = (TileEntityTStamper)par1World.getBlockTileEntity(x, y, z);
		
		if (tileEntity != null) {
			for (int i = 0; i < tileEntity.getSizeInventory(); i++) {
				ItemStack itemstack = tileEntity.getStackInSlot(i);
				
				if (itemstack != null) {
					float f = random.nextFloat() * 0.8F + 0.1F;
					float f1 = random.nextFloat() * 0.8F + 0.1F;
					float f2 = random.nextFloat() * 0.8F + 0.1F;
					
					while (itemstack.stackSize > 0) {
						int j = random.nextInt(21) + 10;
						
						if (j > itemstack.stackSize) {
							j = itemstack.stackSize;
						}
						
						j = 1;
						itemstack.stackSize -= j;
						
						EntityItem item = new EntityItem(par1World, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(itemstack.itemID, j, itemstack.getItemDamage()));
						
						if (itemstack.hasTagCompound()) {
							item.getEntityItem().setTagCompound((NBTTagCompound)itemstack.getTagCompound().copy());
						}
						
						float f3 = 0.05F;
						item.motionX = (double)((float)random.nextGaussian() * f3 * 5F);
						item.motionY = (double)((float)random.nextGaussian() * f3 + 0.2F);
						item.motionZ = (double)((float)random.nextGaussian() * f3 * 5F);
						
						par1World.spawnEntityInWorld(item);
					}
				}
			}
			
			par1World.func_96440_m(x, y, z, par5);
		}
		
		super.breakBlock(par1World, x, y, z, par5, par6);
	}

}
