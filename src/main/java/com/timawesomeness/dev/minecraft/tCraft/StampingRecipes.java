/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class StampingRecipes {
	
	public static ItemStack getStampingResult(int par1) {
		if (par1 == TCraftMain.tIngot.itemID) {
			return new ItemStack(TCraftMain.tIngotBranded);
		} else if (par1 == Item.porkCooked.itemID) {
			return new ItemStack(TCraftMain.tBacon, 10);
		} else {
			return null;
		}
	}
}
