/*
 * Copyright (C) 2014 timawesomeness.
 * Use of this source code is governed by the timawesomeness Open Source License v1.4.
 */
package com.timawesomeness.dev.minecraft.tCraft.entity;

import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;

public class EntityNoodle extends EntityAnimal {
	
	public EntityNoodle(World par1World) {
		super(par1World);
		setSize(0.9F, 0.5F);
		setCustomNameTag("Noodle v" + (Math.round(rand.nextDouble() * 100.0) / 100.0 + rand.nextInt(10)));
		setHealth(7F);
		
		//AI:
		getNavigator().setEnterDoors(true);
		tasks.addTask(0, new EntityAISwimming(this));
		tasks.addTask(1, new EntityAIPanic(this, 0.4F));
		tasks.addTask(2, new EntityAIWander(this, 0.25F));
		tasks.addTask(3, new EntityAIWatchClosest(this, EntityNoodle.class, 6.0F));
		tasks.addTask(4, new EntityAILookIdle(this));
	}

	@Override
	public EntityAgeable createChild(EntityAgeable entityageable) {
		return null;
	}
	
	@Override
	public boolean canBreatheUnderwater() {
		return true;
	}
	
	private double round(double par1, int par2) {
		int mult = 10 ^ par2;
		return Math.floor(par1 * mult + 0.5) / mult;
	}
	
	@Override
	public boolean canDespawn() {
		return false;
	}
	
	@Override
	public boolean isAIEnabled() {
		return true;
	}
	
	@Override
	public int getDropItemId() {
		return TCraftMain.tNoodles.itemID;
		
	}
	
	@Override
	public int getExperiencePoints(EntityPlayer player) {
		return 7;
	}

}
