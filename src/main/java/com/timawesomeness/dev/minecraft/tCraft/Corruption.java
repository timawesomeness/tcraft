//Parts of this class were created by Vazkii, parts of it were created by timawesomeness.
//Probably won't work until 1.7, but who knows?
package com.timawesomeness.dev.minecraft.tCraft;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Random;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.ReflectionHelper;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class Corruption {

	Random rand = new Random();
	
	@SideOnly(Side.CLIENT)
	public void corruptPlayer(EntityClientPlayerMP player) {
		if (player != null && player.ticksExisted > 300) {
			ThreadDownloadImageData tex = player.getTextureSkin();
			BufferedImage img = ReflectionHelper.getPrivateValue(ThreadDownloadImageData.class, tex, 0);
			if (img != null) {
				for (int i = 0; i <1 + (player.ticksExisted - 300) / 30; i++) {
					corruptRandomPixel(tex, img);
				}
			}
		}
	}

	public void corruptRandomPixel(ThreadDownloadImageData tex, BufferedImage img) {
		int width = img.getWidth();
		int height = img.getHeight();

		int x = rand.nextInt(width);
		int y = rand.nextInt(height);
		Color color = new Color(img.getRGB(x, y));
		if(color.getRed() + color.getGreen() + color.getRed() > 0)
			img.setRGB(x, y, color.darker().darker().getRGB());
		ReflectionHelper.setPrivateValue(ThreadDownloadImageData.class, tex, false, 0);
	}

}