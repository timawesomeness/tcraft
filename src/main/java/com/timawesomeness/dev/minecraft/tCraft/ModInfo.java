/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft;

public class ModInfo {
	public static final String modid = "tCraft";
	public static final String name = "tCraft";
	public static final String version = "Alpha 0.0.2";
}
