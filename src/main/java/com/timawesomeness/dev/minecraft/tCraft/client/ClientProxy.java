/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ 
package com.timawesomeness.dev.minecraft.tCraft.client;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.MinecraftForgeClient;

import com.timawesomeness.dev.minecraft.tCraft.CommonProxy;
import com.timawesomeness.dev.minecraft.tCraft.client.model.ModelNoodle;
import com.timawesomeness.dev.minecraft.tCraft.entity.EntityNoodle;
import com.timawesomeness.dev.minecraft.tCraft.renderer.entity.RenderNoodle;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;

public class ClientProxy extends CommonProxy {
	
	@Override
	public void registerRenderers() { //Actually used for rendering stuffs.
		RenderingRegistry.registerEntityRenderingHandler(EntityNoodle.class, new RenderNoodle(new ModelNoodle(), 0.3F));
	}
	
}
