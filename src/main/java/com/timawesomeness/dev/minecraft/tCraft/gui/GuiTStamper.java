/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.gui;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import com.timawesomeness.dev.minecraft.tCraft.ModInfo;
import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;
import com.timawesomeness.dev.minecraft.tCraft.tileentity.TileEntityTStamper;

public class GuiTStamper extends GuiContainer {

	public static final ResourceLocation texture = new ResourceLocation(ModInfo.modid.toLowerCase(), "textures/gui/tstamper.png");
	
	public TileEntityTStamper tStamper;
	
	public GuiTStamper(InventoryPlayer par1InventoryPlayer, TileEntityTStamper par2TileEntity) {
		super(new ContainerTStamper(par1InventoryPlayer, par2TileEntity));
		
		this.tStamper = par2TileEntity;
		
		this.xSize = 176;
		this.ySize = 166;
	}
	
	@Override
	public void drawGuiContainerForegroundLayer(int par1, int par2) {
		String name = TCraftMain.tStamper.getLocalizedName();
		
		fontRenderer.drawString(name, this.xSize / 2 - fontRenderer.getStringWidth(name) / 2, 6, 4210752);
		fontRenderer.drawString(I18n.getString("container.inventory"), 8, this.ySize - 96 + 2, 4210752);
	}
	
	@Override
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		
		int k = tStamper.getStampProgressScaled(15);
		drawTexturedModalRect(guiLeft + 81, guiTop + 34, 176, 0, 14, k);
	}

}
