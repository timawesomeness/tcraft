/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.blocks;

import java.util.Random;

import net.minecraft.block.BlockStone;
import net.minecraft.client.renderer.texture.IconRegister;

import com.timawesomeness.dev.minecraft.tCraft.ModInfo;
import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockTStone extends BlockStone {

	public BlockTStone(int par1) {
		super(par1);
		setCreativeTab(TCraftMain.tabTCraft);
		setHardness(0.5F);
		setStepSound(soundStoneFootstep);
	}
	
	 @SideOnly(Side.CLIENT) 
     public void registerIcons(IconRegister par1IconRegister)
     {
         this.blockIcon = par1IconRegister.registerIcon(ModInfo.modid + ":" + this.getUnlocalizedName().substring(5));
     }
	 
	 public int idDropped(int par1, Random par2Random, int par3) {
		 return TCraftMain.tCobble.blockID;
	 }
	 
	 public boolean canSilkHarvet() {
		 return true;
	 }
}
