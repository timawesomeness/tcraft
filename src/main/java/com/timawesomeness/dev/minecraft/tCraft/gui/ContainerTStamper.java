/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.tileentity.TileEntityFurnace;

import com.timawesomeness.dev.minecraft.tCraft.StampingRecipes;
import com.timawesomeness.dev.minecraft.tCraft.tileentity.TileEntityTStamper;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerTStamper extends Container {
	
	private TileEntityTStamper tileEntityTStamper;

	public int lastStampTime;
	
	public ContainerTStamper(InventoryPlayer par1InventoryPlayer, TileEntityTStamper par2TileEntity) {
		tileEntityTStamper = par2TileEntity;
		
		addSlotToContainer(new Slot(par2TileEntity, 0, 60, 33));
		addSlotToContainer(new SlotFurnace(par1InventoryPlayer.player, par2TileEntity, 1, 100, 33));
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				this.addSlotToContainer(new Slot(par1InventoryPlayer, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}
		
		for (int i = 0; i < 9; i++) {
			addSlotToContainer(new Slot(par1InventoryPlayer, i, 8 + i * 18, 142));
		}
	}
	
	public void addCraftingToCrafters(ICrafting par1Crafting) {
		super.addCraftingToCrafters(par1Crafting);
		par1Crafting.sendProgressBarUpdate(this, 0, tileEntityTStamper.stampTime);
		//par1Crafting.sendProgressBarUpdate(this, 1, tileEntityTStamper.stampTime);
	}
	
	public void detectAndSendChanges() {
		super.detectAndSendChanges();
		
		for (int i =0; i < crafters.size(); i++) {
			ICrafting icrafting = (ICrafting)crafters.get(i);
			
			if (lastStampTime != tileEntityTStamper.stampTime) {
				icrafting.sendProgressBarUpdate(this, 0, tileEntityTStamper.stampTime);
			}
		}
		
		lastStampTime = tileEntityTStamper.stampTime;
	}
	
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int par1, int par2) {
		if (par1 == 0) tileEntityTStamper.stampTime = par2;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2Slot) {
		ItemStack itemstack = null;
		Slot slot = (Slot) inventorySlots.get(par2Slot);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (par2Slot == 1) {
				if (!mergeItemStack(itemstack1, 2, 38, true)) {
					return null;
				}

				slot.onSlotChange(itemstack1, itemstack);
			} else if (par2Slot != 0) {
				if (StampingRecipes.getStampingResult(itemstack1.itemID) != null) {
					if (!mergeItemStack(itemstack1, 0, 1, true)) {
						return null;
					}
				} else if (par2Slot >= 2 && par2Slot < 29) {
					if (!mergeItemStack(itemstack1, 29, 38, false)) {
						return null;
					}
				} else if (par2Slot >= 29 && par2Slot < 38 && !mergeItemStack(itemstack1, 2, 29, false)) {
					return null;
				}
			} else if (!mergeItemStack(itemstack1, 2, 38, false)) {
				return null;
			}

			if (itemstack1.stackSize == 0) {
				slot.putStack((ItemStack) null);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize) {
				return null;
			}

			slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
		}

		return itemstack;
	}

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return tileEntityTStamper.isUseableByPlayer(entityplayer);
	}

}
