/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.world;

import java.util.Random;

import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;

import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import cpw.mods.fml.common.IWorldGenerator;

public class TCraftWorldGenerator implements IWorldGenerator {

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		switch(world.provider.dimensionId) {
		case -1:
			generateNether(world, random, chunkX * 16, chunkZ * 16);
			break;
		case 0:
			generateSurface(world, random, chunkX * 16, chunkZ * 16);
			break;
		case 1:
			generateEnd(world, random, chunkX * 16, chunkZ * 16);
		}
	}

	private void generateEnd(World world, Random random, int i, int j) { }

	private void generateSurface(World world, Random random, int i, int j) {
		for(int k = 0; k < 10; k++) {
			int tOreXCoord = i + random.nextInt(16);
			int tOreYCoord = random.nextInt(37);
			int tOreZCoord = j + random.nextInt(16);
			
			(new WorldGenMinable(TCraftMain.tOreID, 8)).generate(world, random, tOreXCoord, tOreYCoord, tOreZCoord);
		}
	}

	private void generateNether(World world, Random random, int i, int j) {	}
	
}
