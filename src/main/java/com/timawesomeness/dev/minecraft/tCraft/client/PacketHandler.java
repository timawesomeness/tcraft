/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.client;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import net.minecraft.client.Minecraft;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class PacketHandler implements IPacketHandler {
	@Override
	public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player) {
		switch (Integer.parseInt(packet.channel)) {
			case 0:
				handleParticlePacket(packet);
				break;
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void handleParticlePacket(Packet250CustomPayload thePacket) {
		ByteArrayInputStream bin = new ByteArrayInputStream(thePacket.data);
		DataInputStream din = new DataInputStream(bin);
		try {
			double x = din.readDouble();
			double y = din.readDouble();
			double z = din.readDouble();
			Minecraft.getMinecraft().theWorld.spawnParticle("largeexplode", x, y, z, 0, 0, 0);
			Minecraft.getMinecraft().theWorld.playSound(x, y, z, "random.anvil_land", 1F, 0F, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}