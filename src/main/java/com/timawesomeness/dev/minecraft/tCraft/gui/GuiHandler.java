/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.gui;

import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;
import com.timawesomeness.dev.minecraft.tCraft.tileentity.TileEntityTStamper;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {
	
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);

		if (tileEntity != null) {
			switch (ID) {
				case TCraftMain.tStamperGuiID:
					if (tileEntity instanceof TileEntityTStamper) {
						return new ContainerTStamper(player.inventory, (TileEntityTStamper)tileEntity);
					}
			}
		}

		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		
		if (tileEntity != null) {
			switch (ID) {
				case TCraftMain.tStamperGuiID:
					if (tileEntity instanceof TileEntityTStamper) {
						return new GuiTStamper(player.inventory, (TileEntityTStamper)tileEntity);
					}
			}
		}
		
		return null;
	}
	
}
