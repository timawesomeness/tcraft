/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.registry.GameRegistry;

public class RecipeRegistration {

	public static void registerRecipes() {
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tPickaxe), "xxx", " y ", " y ", 'x', new ItemStack(TCraftMain.tIngot), 'y', new ItemStack(Item.stick));
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tBlock), "xxx", "xxx", "xxx", 'x', new ItemStack(TCraftMain.tIngot));
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tDirt, 8), "xxx", "xyx", "xxx", 'x', new ItemStack(Block.dirt), 'y', new ItemStack(TCraftMain.tIngot));
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tStone, 8), "xxx", "xyx", "xxx", 'x', new ItemStack(Block.stone), 'y', new ItemStack(TCraftMain.tIngot));
		GameRegistry.addSmelting(TCraftMain.tOreID, new ItemStack(TCraftMain.tIngot), 0.5F);
		GameRegistry.addShapelessRecipe(new ItemStack(TCraftMain.tIngot, 9), new ItemStack(TCraftMain.tBlock));
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tHelmet), "xxx", "x x", 'x', new ItemStack(TCraftMain.tIngot));
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tChestplate), "x x", "xxx", "xxx", 'x', new ItemStack(TCraftMain.tIngot));
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tLeggings), "xxx", "x x", "x x", 'x', new ItemStack(TCraftMain.tIngot));
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tBoots), "x x", "x x", 'x', new ItemStack(TCraftMain.tIngot));
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tStamp), " x ", " x ", "xxx", 'x', new ItemStack(Item.ingotIron));
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tStamper), " x ", "yzy", "www", 'x', new ItemStack(TCraftMain.tStamp), 'y', new ItemStack(Item.ingotIron), 'z', new ItemStack(Block.anvil),'w', new ItemStack(TCraftMain.tBlock));
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tCobble, 8), "xxx", "xyx", "xxx", 'x', new ItemStack(Block.cobblestone), 'y', new ItemStack(TCraftMain.tIngot));
		GameRegistry.addSmelting(TCraftMain.tCobble.blockID, new ItemStack(TCraftMain.tStone), 0.2F);
		GameRegistry.addShapedRecipe(new ItemStack(TCraftMain.tPortal), "xyx", "zba", "cdc", 'x', new ItemStack(TCraftMain.tIngotBranded),'y', new ItemStack(Block.obsidian),'z', new ItemStack(TCraftMain.tStone), 'b', new ItemStack(Item.emerald), 'a', new ItemStack(TCraftMain.tDirt), 'c', new ItemStack(TCraftMain.tBlock), 'd', new ItemStack(Block.blockDiamond));
		GameRegistry.addShapelessRecipe(new ItemStack(TCraftMain.tNoodleFood), new ItemStack(TCraftMain.tNoodles), new ItemStack(TCraftMain.tBacon));
	}

}
