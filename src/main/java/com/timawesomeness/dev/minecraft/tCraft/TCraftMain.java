/*
 * Copyright (C) 2014 timawesomeness.
 * Use of this source code is governed by the timawesomeness Open Source License v1.4.
 */

package com.timawesomeness.dev.minecraft.tCraft;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.EnumHelper;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.oredict.OreDictionary;

import com.timawesomeness.dev.minecraft.tCraft.blocks.BlockTBlock;
import com.timawesomeness.dev.minecraft.tCraft.blocks.BlockTCobble;
import com.timawesomeness.dev.minecraft.tCraft.blocks.BlockTDirt;
import com.timawesomeness.dev.minecraft.tCraft.blocks.BlockTOre;
import com.timawesomeness.dev.minecraft.tCraft.blocks.BlockTPortal;
import com.timawesomeness.dev.minecraft.tCraft.blocks.BlockTRail;
import com.timawesomeness.dev.minecraft.tCraft.blocks.BlockTSapling;
import com.timawesomeness.dev.minecraft.tCraft.blocks.BlockTStamper;
import com.timawesomeness.dev.minecraft.tCraft.blocks.BlockTStone;
import com.timawesomeness.dev.minecraft.tCraft.client.PacketHandler;
import com.timawesomeness.dev.minecraft.tCraft.command.SlapCommand;
import com.timawesomeness.dev.minecraft.tCraft.entity.EntityNoodle;
import com.timawesomeness.dev.minecraft.tCraft.gui.GuiHandler;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTAxe;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTBacon;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTBoots;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTChestplate;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTHelmet;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTHoe;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTIngot;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTIngotBranded;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTLeggings;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTNoodles;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTPickaxe;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTShovel;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTNoodleFood;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTStamp;
import com.timawesomeness.dev.minecraft.tCraft.items.ItemTSword;
import com.timawesomeness.dev.minecraft.tCraft.tileentity.TileEntityPortal;
import com.timawesomeness.dev.minecraft.tCraft.tileentity.TileEntityTStamper;
import com.timawesomeness.dev.minecraft.tCraft.world.BiomeTBiome;
import com.timawesomeness.dev.minecraft.tCraft.world.BonemealEventChecker;
import com.timawesomeness.dev.minecraft.tCraft.world.TCraftTreeGenerator;
import com.timawesomeness.dev.minecraft.tCraft.world.TCraftWorldGenerator;
import com.timawesomeness.dev.minecraft.tCraft.world.TDimensionWorldProvider;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = ModInfo.modid, name = ModInfo.name, version = ModInfo.version)
@NetworkMod(clientSideRequired = true, serverSideRequired = false, channels = {"0"}, packetHandler = PacketHandler.class) //CHANNEL MUST BE A NUMBER!!! (because gradle is stupid sometimes.)
public class TCraftMain {

	@Instance(value = ModInfo.modid)
	public static TCraftMain instance;
	@SidedProxy(serverSide = "com.timawesomeness.dev.minecraft.tCraft.CommonProxy", clientSide = "com.timawesomeness.dev.minecraft.tCraft.client.ClientProxy")
	public static CommonProxy proxy;

	// Blocks:
	public static Block tOre;
	public static Block tBlock;
	public static Block tDirt;
	public static Block tPortal;
	public static Block tStone;
	public static Block tSapling;
	public static Block tCobble;
	public static Block	tStamper;
	public static Block	tRail;

	// Items:
	public static Item tIngot;
	public static Item tIngotBranded;
	public static Item tPickaxe;
	public static Item tSword;
	public static Item tShovel;
	public static Item tAxe;
	public static Item tHoe;
	public static Item tHelmet;
	public static Item tChestplate;
	public static Item tLeggings;
	public static Item tBoots;
	public static Item tBacon;
	public static Item tStamp;
	public static Item tNoodles;
	public static Item tNoodleFood;

	// IDs:
	public static int tOreID;
	public static int tBlockID = 180;
	public static int tDirtID = 181;
	public static int tIngotID;
	public static int tIngotBrandedID;
	public static int tPickaxeID;
	public static int tSwordID;
	public static int tShovelID;
	public static int tAxeID;
	public static int tHoeID;
	public static int tPortalID;
	public static int tStoneID = 182;
	public static int tSaplingID = 183;
	public static int tCobbleID;
	public static int tHelmetID;
	public static int tChestplateID;
	public static int tLeggingsID;
	public static int tBootsID;
	public static int tBaconID;
	public static int tStamperID;
	public static int tStampID;
	public static final int tStamperGuiID = 0;
	public static int tRailID;
	public static int tNoodlesID;
	public static int tNoodleFoodID;

	// Creative Tab:
	public static CreativeTabs tabTCraft = new CreativeTabs("tabTCraft") {
		public ItemStack getIconItemStack() {
			return new ItemStack(tIngot, 1, 0);
		}
	};

	// Materials:
	public static EnumToolMaterial tIngotMat = EnumHelper.addToolMaterial("t Ingot", 2, 500, 6.5F, 2.5F, 15);
	public static EnumArmorMaterial tIngotArmMat = EnumHelper.addArmorMaterial("t Ingot", 450, new int[] {3, 7, 5, 2}, 30);

	// Dimension:
	public static int tDimensionID;

	// Biome:
	public static BiomeGenBase tBiome;
	
	//Other:
	public static MinecraftServer server;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		System.out.println("[tCraft] tCraft loading started...");
		System.out.println("[tCraft] tCraft is Copyright (C) 2014 timawesomeness.");
		
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		config.load();

		tOreID = config.getBlock("tOre", 1770).getInt();
		tPortalID = config.getBlock("tPortal", 1773).getInt();
		tIngotID = config.getItem("tIngot", 3870).getInt();
		tIngotBrandedID = config.getItem("tIngotBranded", 3871).getInt();
		tPickaxeID = config.getItem("tPickaxe", 3872).getInt();
		tSwordID = config.getItem("tSword", 3873).getInt();
		tShovelID = config.getItem("tShovel", 3874).getInt();
		tAxeID = config.getItem("tAxe", 3875).getInt();
		tHoeID = config.getItem("tHoe", 3876).getInt();
		tDimensionID = config.get("Dimensions", "tDimension", 9).getInt();
		tCobbleID = config.getBlock("tCobble", 1771).getInt();
		tHelmetID = config.getItem("tHelmet", 3877).getInt();
		tChestplateID = config.getItem("tChestplate", 3878).getInt();
		tLeggingsID = config.getItem("tLeggings", 3879).getInt();
		tBootsID = config.getItem("tBoots", 3880).getInt();
		tBaconID = config.getItem("tBacon", 3881).getInt();
		tStamperID = config.getBlock("tStamper", 1772).getInt();
		tStampID = config.getItem("tStamp", 3882).getInt();
		tRailID = config.getBlock("tRail", 1773).getInt();
		tNoodlesID = config.getItem("tNoodles", 3883).getInt();
		tNoodleFoodID = config.getItem("tNoodleFood", 3884).getInt();
		
		config.save();
	}

	@EventHandler
	public void load(FMLInitializationEvent event) {
		System.out.printf("[tCraft] tCraft version %s initializing...\n", ModInfo.version);
		
		// Blocks:
		tOre = new BlockTOre(tOreID, Material.rock).setUnlocalizedName("tOre");
		MinecraftForge.setBlockHarvestLevel(tOre, "pickaxe", 2);
		GameRegistry.registerBlock(tOre, ModInfo.modid + tOre.getUnlocalizedName().substring(5));

		tBlock = new BlockTBlock(tBlockID, Material.iron).setUnlocalizedName("tBlock");
		MinecraftForge.setBlockHarvestLevel(tBlock, "pickaxe", 2);
		GameRegistry.registerBlock(tBlock, ModInfo.modid + tBlock.getUnlocalizedName().substring(5));

		tDirt = new BlockTDirt(tDirtID, Material.ground).setUnlocalizedName("tDirt");
		MinecraftForge.setBlockHarvestLevel(tDirt, "shovel", 0);
		GameRegistry.registerBlock(tDirt, ModInfo.modid + tDirt.getUnlocalizedName().substring(5));
		OreDictionary.registerOre("dirt", new ItemStack(tDirt));

		tPortal = new BlockTPortal(tPortalID, Material.rock).setUnlocalizedName("tPortal");
		MinecraftForge.setBlockHarvestLevel(tPortal, "pickaxe", 2);
		GameRegistry.registerBlock(tPortal, ModInfo.modid + tPortal.getUnlocalizedName().substring(5));

		tStone = new BlockTStone(tStoneID).setUnlocalizedName("tStone");
		MinecraftForge.setBlockHarvestLevel(tStone, "pickaxe", 1);
		GameRegistry.registerBlock(tStone, ModInfo.modid + tStone.getUnlocalizedName().substring(5));

		tSapling = new BlockTSapling(tSaplingID).setUnlocalizedName("tSapling");
		GameRegistry.registerBlock(tSapling, ModInfo.modid + tSapling.getUnlocalizedName().substring(5));
		
		tCobble = new BlockTCobble(tCobbleID, Material.rock).setUnlocalizedName("tCobble");
		MinecraftForge.setBlockHarvestLevel(tCobble, "pickaxe", 1);
		GameRegistry.registerBlock(tCobble, ModInfo.modid + tCobble.getUnlocalizedName().substring(5));
		
		tStamper = new BlockTStamper(tStamperID, Material.piston).setUnlocalizedName("tStamper");
		MinecraftForge.setBlockHarvestLevel(tStamper, "pickaxe", 1);
		GameRegistry.registerBlock(tStamper, ModInfo.modid + tStamper.getUnlocalizedName().substring(5));
		
		tRail = new BlockTRail(tRailID).setUnlocalizedName("tRail");
		MinecraftForge.setBlockHarvestLevel(tRail, "pickaxe", 0);
		GameRegistry.registerBlock(tRail, ModInfo.modid + tRail.getUnlocalizedName().substring(5));

		// Items:
		tIngot = new ItemTIngot(tIngotID).setUnlocalizedName("tIngot");
		GameRegistry.registerItem(tIngot, ModInfo.modid + tIngot.getUnlocalizedName().substring(5));

		tIngotBranded = new ItemTIngotBranded(tIngotBrandedID).setUnlocalizedName("tIngotBranded");
		GameRegistry.registerItem(tIngotBranded, ModInfo.modid + tIngotBranded.getUnlocalizedName().substring(5));

		tPickaxe = new ItemTPickaxe(tPickaxeID, tIngotMat).setUnlocalizedName("tPickaxe");
		GameRegistry.registerItem(tPickaxe, ModInfo.modid + tPickaxe.getUnlocalizedName().substring(5));

		tSword = new ItemTSword(tSwordID, tIngotMat).setUnlocalizedName("tSword");
		GameRegistry.registerItem(tSword, ModInfo.modid + tSword.getUnlocalizedName().substring(5));

		tShovel = new ItemTShovel(tShovelID, tIngotMat).setUnlocalizedName("tShovel");
		GameRegistry.registerItem(tShovel, ModInfo.modid + tShovel.getUnlocalizedName().substring(5));

		tAxe = new ItemTAxe(tAxeID, tIngotMat).setUnlocalizedName("tAxe");
		GameRegistry.registerItem(tAxe, ModInfo.modid + tAxe.getUnlocalizedName().substring(5));

		tHoe = new ItemTHoe(tHoeID, tIngotMat).setUnlocalizedName("tHoe");
		GameRegistry.registerItem(tHoe, ModInfo.modid + tHoe.getUnlocalizedName().substring(5));
		
		tHelmet = new ItemTHelmet(tHelmetID, tIngotArmMat, 0, 0).setUnlocalizedName("tHelmet");
		GameRegistry.registerItem(tHelmet, ModInfo.modid + tHelmet.getUnlocalizedName().substring(5));
		
		tChestplate = new ItemTChestplate(tChestplateID, tIngotArmMat, 0, 1).setUnlocalizedName("tChestplate");
		GameRegistry.registerItem(tChestplate, ModInfo.modid + tChestplate.getUnlocalizedName().substring(5));
		
		tLeggings = new ItemTLeggings(tLeggingsID, tIngotArmMat, 0, 2).setUnlocalizedName("tLeggings");
		GameRegistry.registerItem(tLeggings, ModInfo.modid + tLeggings.getUnlocalizedName().substring(5));
		
		tBoots = new ItemTBoots(tBootsID, tIngotArmMat, 0, 3).setUnlocalizedName("tBoots");
		GameRegistry.registerItem(tBoots, ModInfo.modid + tBoots.getUnlocalizedName().substring(5));
		
		tBacon = new ItemTBacon(tBaconID, 2, 0.4F, true).setUnlocalizedName("tBacon");
		GameRegistry.registerItem(tBacon, ModInfo.modid + tBacon.getUnlocalizedName().substring(5));
		OreDictionary.registerOre("itemBacon", tBacon);
		
		tStamp = new ItemTStamp(tStampID).setUnlocalizedName("tStamp");
		GameRegistry.registerItem(tStamp, ModInfo.modid + tStamp.getUnlocalizedName().substring(5));
		
		tNoodles = new ItemTNoodles(tNoodlesID, 2, 0.3F, false).setUnlocalizedName("tNoodles");
		GameRegistry.registerItem(tNoodles, ModInfo.modid + tNoodles.getUnlocalizedName().substring(5));
		OreDictionary.registerOre("itemNoodles", tNoodles);
		
		tNoodleFood = new ItemTNoodleFood(tNoodleFoodID, 20, 10F, false).setUnlocalizedName("tNoodleFood");
		GameRegistry.registerItem(tNoodleFood, ModInfo.modid + tNoodleFood.getUnlocalizedName().substring(5));
		
		// Language:
		LanguageRegistration langReg = new LanguageRegistration();
		langReg.registerLangThings();

		// Recipes:
		RecipeRegistration.registerRecipes();

		// WorldGen:
		tBiome = new BiomeTBiome(25);
		GameRegistry.registerWorldGenerator(new TCraftWorldGenerator());
		GameRegistry.registerWorldGenerator(new TCraftTreeGenerator(false));
		DimensionManager.registerProviderType(tDimensionID, TDimensionWorldProvider.class, false);
		DimensionManager.registerDimension(tDimensionID, tDimensionID);

		// Other:
		tIngotMat.customCraftingMaterial = tIngot;
		MinecraftForge.EVENT_BUS.register(new BonemealEventChecker());
		GameRegistry.registerTileEntity(TileEntityTStamper.class, "tileEntityTStamper");
		GameRegistry.registerTileEntity(TileEntityPortal.class, "TileEntityPortal");
		NetworkRegistry.instance().registerGuiHandler(this, new GuiHandler());
		EntityRegistry.registerGlobalEntityID(EntityNoodle.class, "Noodle", EntityRegistry.findGlobalUniqueEntityId(), 0xFDD66C, 0xFDD44C);
		proxy.registerRenderers();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		System.out.printf("[tCraft] tCraft version %s done loading.", ModInfo.version);
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) {
		server = FMLCommonHandler.instance().getMinecraftServerInstance();
		event.registerServerCommand(new SlapCommand());
	}
}