/*
 * Copyright (C) 2014 timawesomeness.
 * Use of this source code is governed by the timawesomeness Open Source License v1.4.
 */
package com.timawesomeness.dev.minecraft.tCraft.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRailBase;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockTRail extends BlockRailBase {
	@SideOnly(Side.CLIENT)
	private Icon theIcon;

	public BlockTRail(int par1) {
		super(par1, false);
	}

	@SideOnly(Side.CLIENT)
	/**
	 * From the specified side and block metadata retrieves the blocks texture. Args: side, metadata
	 */
	public Icon getIcon(int par1, int par2) {
		return par2 >= 6 ? this.theIcon : this.blockIcon;
	}

	@SideOnly(Side.CLIENT)
	/**
	 * When this method is called, your block should register all the icons it needs with the given IconRegister. This
	 * is the only chance you get to register icons.
	 */
	public void registerIcons(IconRegister par1IconRegister) {
		super.registerIcons(par1IconRegister);
		this.theIcon = par1IconRegister.registerIcon(this.getTextureName() + "_turned");
	}

	protected void func_94358_a(World par1World, int par2, int par3, int par4, int par5, int par6, int par7) {
		if (par7 > 0 && Block.blocksList[par7].canProvidePower() && (new BlockBaseTRailLogic(this, par1World, par2, par3, par4)).getNumberOfAdjacentTracks() == 3) {
			this.refreshTrackShape(par1World, par2, par3, par4, false);
		}
	}

	@Override
	public void onNeighborBlockChange(World par1World, int par2, int par3, int par4, int par5) {
		if (!par1World.isRemote) {
			int i1 = par1World.getBlockMetadata(par2, par3, par4);
			int j1 = i1;

			if (this.isPowered) {
				j1 = i1 & 7;
			}

			boolean flag = false;

			if (!par1World.doesBlockHaveSolidTopSurface(par2, par3 - 1, par4)) {
				flag = true;
			}

			if (j1 == 2 && !par1World.doesBlockHaveSolidTopSurface(par2 + 1, par3, par4)) {
				flag = true;
			}

			if (j1 == 3 && !par1World.doesBlockHaveSolidTopSurface(par2 - 1, par3, par4)) {
				flag = true;
			}

			if (j1 == 4 && !par1World.doesBlockHaveSolidTopSurface(par2, par3, par4 - 1)) {
				flag = true;
			}

			if (j1 == 5 && !par1World.doesBlockHaveSolidTopSurface(par2, par3, par4 + 1)) {
				flag = true;
			}

			if (flag) {
				this.dropBlockAsItem(par1World, par2, par3, par4, par1World.getBlockMetadata(par2, par3, par4), 0);
				par1World.setBlockToAir(par2, par3, par4);
			} else {
				this.func_94358_a(par1World, par2, par3, par4, i1, j1, par5);
			}
		}
	}
}
