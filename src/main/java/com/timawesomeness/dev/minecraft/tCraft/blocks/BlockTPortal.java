/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.blocks;

import com.timawesomeness.dev.minecraft.tCraft.ModInfo;
import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;
import com.timawesomeness.dev.minecraft.tCraft.TDimensionTeleporter;
import com.timawesomeness.dev.minecraft.tCraft.tileentity.TileEntityPortal;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockTPortal extends Block {

	public BlockTPortal(int par1, Material par2Material) {
		super(par1, par2Material);
		setCreativeTab(TCraftMain.tabTCraft);
		setHardness(8.0F);
		setStepSound(soundMetalFootstep);
	}

	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister par1IconRegister) {
		this.blockIcon = par1IconRegister.registerIcon(ModInfo.modid + ":" + this.getUnlocalizedName().substring(5));
	}
	
	@Override
	public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9) {
		if ((par5EntityPlayer.ridingEntity == null) && (par5EntityPlayer.riddenByEntity == null) && ((par5EntityPlayer instanceof EntityPlayerMP))) {
			EntityPlayerMP thePlayer = (EntityPlayerMP) par5EntityPlayer;
			if (thePlayer.dimension != TCraftMain.tDimensionID) {
				thePlayer.setLocationAndAngles(par2 + 0.5D, thePlayer.posY, par4 + 0.5D, thePlayer.rotationYaw, thePlayer.rotationPitch);
				thePlayer.mcServer.getConfigurationManager().transferPlayerToDimension(thePlayer, TCraftMain.tDimensionID, new TDimensionTeleporter(thePlayer.mcServer.worldServerForDimension(TCraftMain.tDimensionID)));
			} else {
				thePlayer.mcServer.getConfigurationManager().transferPlayerToDimension(thePlayer, 0, new TDimensionTeleporter(thePlayer.mcServer.worldServerForDimension(0)));
			}
			return true;
		}
		return true;
	}
	
	@Override
	public boolean hasTileEntity(int metadata) {
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(World par1World, int metadata) {
		return new TileEntityPortal();
	}

}
