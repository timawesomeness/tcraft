/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.world;

import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.SpawnListEntry;
import net.minecraft.world.gen.feature.WorldGenerator;

import com.timawesomeness.dev.minecraft.tCraft.ModInfo;
import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;
import com.timawesomeness.dev.minecraft.tCraft.entity.EntityNoodle;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BiomeTBiome extends BiomeGenBase {

	private WorldGenerator treeGenerator;
	public final Material blockMaterial;
	
	public BiomeTBiome(int par1) {
		super(par1);
		this.blockMaterial = Material.water;
		this.minHeight = 0.1F;
		this.maxHeight = 1.7F;
		this.spawnableMonsterList.clear();
		this.spawnableCreatureList.clear();
		this.spawnableCaveCreatureList.clear();
		this.spawnableWaterCreatureList.clear();
		this.spawnableCreatureList.add(new SpawnListEntry(EntityNoodle.class, 2, 1, 3));
		this.topBlock = ((byte)TCraftMain.tDirt.blockID);
		this.fillerBlock = ((byte)TCraftMain.tDirt.blockID);
		this.setBiomeName("The t Biome");
		this.waterColorMultiplier = 0x00FF00;
		this.setColor(0x00FF00);
		this.theBiomeDecorator.treesPerChunk = 1;
		this.treeGenerator = new TCraftTreeGenerator(false);
		this.setDisableRain();
	}
	
	public WorldGenerator getRandomWorldGenForTrees(Random par1Random)
	{
		return (WorldGenerator)(par1Random.nextInt(5) == 0 ? this.worldGeneratorForest : (par1Random.nextInt(10) == 0 ? this.treeGenerator : this.worldGeneratorTrees));
	}
	
	@SideOnly(Side.CLIENT)
	public int getSkyColorByTemp(float par1) {
		return 0x00ff00;
	}

}
