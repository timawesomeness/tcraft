/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.world;

import java.awt.Event;
import java.util.ArrayList;
import java.util.Random;

import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;
import com.timawesomeness.dev.minecraft.tCraft.blocks.BlockTSapling;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.BonemealEvent;

public class BonemealEventChecker
{
	private int BlockID;
	/** Used to make the sapling grow the tree **/
	@ForgeSubscribe
	public void bonemealUsed(BonemealEvent event)
	{
		if(event.world.getBlockId(event.X, event.Y, event.Z) == TCraftMain.tSapling.blockID)
		{
			((BlockTSapling)TCraftMain.tSapling).growTree(event.world, event.X, event.Y, event.Z, event.world.rand);
		}
	}
}