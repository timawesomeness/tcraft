/*
 * Copyright (C) 2014 timawesomeness.
 * Use of this source code is governed by the timawesomeness Open Source License v1.4.
 */
package com.timawesomeness.dev.minecraft.tCraft.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import com.timawesomeness.dev.minecraft.tCraft.Corruption;
import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.dedicated.DedicatedServer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;

public class SlapCommand implements ICommand {

	@Override
	public int compareTo(Object arg0) {
		return 0;
	}

	@Override
	public String getCommandName() {
		return "slap";
	}

	@Override
	public String getCommandUsage(ICommandSender icommandsender) {
		return "/slap [player]";
	}

	@Override
	public List getCommandAliases() {
		return null;
	}

	@Override
	public void processCommand(ICommandSender icommandsender, String[] astring) {
		String players[] = TCraftMain.server.getAllUsernames();
		
		for (String s: players) {
			EntityPlayerMP player = TCraftMain.server.getConfigurationManager().getPlayerForUsername(s);
			
			if (astring.length == 0) {
				player.addChatMessage("* timawesomeness slaps " + ((EntityPlayer)icommandsender).username + " with a giant trout because they asked to be.");
			} else {
				player.addChatMessage("* timawesomeness slaps " + EnumChatFormatting.GREEN + astring[0] + EnumChatFormatting.WHITE + " with a giant trout, courtesy of " + EnumChatFormatting.AQUA + ((EntityPlayer)icommandsender).username + EnumChatFormatting.WHITE + ".");
			}	
		}
	}

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender icommandsender) {
		return true;
	}

	@Override
	public List addTabCompletionOptions(ICommandSender icommandsender, String[] astring) {
		return Arrays.asList(TCraftMain.server.getAllUsernames());
	}

	@Override
	public boolean isUsernameIndex(String[] astring, int i) {
		return false;
	}

}
