/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */package com.timawesomeness.dev.minecraft.tCraft;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.LongHashMap;
import net.minecraft.util.MathHelper;
import net.minecraft.world.PortalPosition;
import net.minecraft.world.Teleporter;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;

import com.timawesomeness.dev.minecraft.tCraft.tileentity.TileEntityPortal;

public class TDimensionTeleporter extends Teleporter {
	private final WorldServer worldServer;
	private final Random rand;
	private final LongHashMap longHashMap = new LongHashMap();
	private final List arrayList = new ArrayList();

	public TDimensionTeleporter(WorldServer par1WorldServer) {
		super(par1WorldServer);
		this.worldServer = par1WorldServer;
		this.rand = new Random(par1WorldServer.getSeed());
	}

	@Override
	public void placeInPortal(Entity entity, double x, double y, double z, float r) {
		if (!placeInExistingPortal(entity, x, y, z, r)) {
			if (this.worldServer.provider.dimensionId != TCraftMain.tDimensionID) {
				y = this.worldServer.getTopSolidOrLiquidBlock((int) x, (int) z);
				entity.setLocationAndAngles(x, y, z, entity.rotationYaw, 0.0F);
			} else {
				makePortal(entity);
			}
		} else {
			placeInExistingPortal(entity, x, y, z, r);
		}
	}

	public TileEntity findPortalInChunk(double x, double z) {
		Chunk chunk = this.worldServer.getChunkFromBlockCoords((int) x, (int) z);
		Iterator t = chunk.chunkTileEntityMap.values().iterator();
		while (t.hasNext()) {
			Object tile = t.next();
			if (tile instanceof TileEntityPortal) {
				return (TileEntity) tile;
			}
		}
		return null;
	}

	@Override
	public boolean placeInExistingPortal(Entity entity, double x, double y, double z, float r) {
		TileEntity destPortal = null;
		if (destPortal == null) {
			for (int s = 0; (s <= 5) && (destPortal == null); s++) {
				for (int dx = -s; dx <= s; dx++) {
					for (int dz = -s; dz <= s; dz++) {
						if (destPortal == null) {
							destPortal = findPortalInChunk(x + dx * 16, z + dz * 16);
						}
					}
				}
			}
		}
		if (destPortal != null) {
			entity.setLocationAndAngles(destPortal.xCoord + 0.5D, destPortal.yCoord + 1, destPortal.zCoord + 0.5D, entity.rotationYaw, entity.rotationPitch);
			entity.motionX = (entity.motionY = entity.motionZ = 0.0D);
			return true;
		}
		return false;
	}

	public boolean makePortal(Entity entity) {
		int ex = MathHelper.floor_double(entity.posX);
		int ey = 3;
		int ez = MathHelper.floor_double(entity.posZ);

		for (int x = -1; x <= 1; x++) {
			for (int z = -1; z <= 1; z++) {
				for (int y = -1; y <= 3; y++) {
					if ((x == 0) && (y == -1) && (z == 0)) {
						this.worldServer.setBlock(ex + x, ey + y, ez + z, TCraftMain.tPortal.blockID, 0, 2);
						this.worldServer.scheduleBlockUpdate(ex + x, ey + y, ez + z, TCraftMain.tPortal.blockID, 1);
					} else if ((x == -1) || (x == 1) || (y <= -1) || (y == 3) || (z == -1) || (z == 1)) {
						this.worldServer.setBlock(ex + x, ey + y, ez + z, TCraftMain.tOre.blockID);
					} else if (y == 2) {
						this.worldServer.setBlock(ex + x, ey + y, ez + z, Block.glowStone.blockID);
					} else {
						this.worldServer.setBlock(ex + x, ey + y, ez + z, 0);
					}
				}
			}
		}
		entity.setLocationAndAngles(ex + 0.5D, ey, ez + 0.5D, entity.rotationYaw, 0.0F);
		entity.motionX = (entity.motionY = entity.motionZ = 0.0D);

		return true;
	}

	@Override
	public void removeStalePortalLocations(long par1) {
		if (par1 % 100L == 0L) {
			Iterator iterator = this.arrayList.iterator();
			long j = par1 - 600L;
			while (iterator.hasNext()) {
				Long olong = (Long) iterator.next();
				PortalPosition portalposition = (PortalPosition) this.longHashMap.getValueByKey(olong.longValue());
				if ((portalposition == null) || (portalposition.lastUpdateTime < j)) {
					iterator.remove();
					this.longHashMap.remove(olong.longValue());
				}
			}
		}
	}
}
