/*
 * Copyright (C) 2014 timawesomeness.
 * Use of this source code is governed by the timawesomeness Open Source License v1.4.
 */
package com.timawesomeness.dev.minecraft.tCraft.items;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

import com.timawesomeness.dev.minecraft.tCraft.ModInfo;
import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemTNoodleFood extends ItemFood {
	/** Number of ticks to run while 'EnumAction'ing until result. */
	public final int itemUseDuration;

	/** The amount this food item heals the player. */
	private final int healAmount;
	private final float saturationModifier;

	/** Whether wolves like this food (true for raw and cooked porkchop). */
	private final boolean isWolfsFavoriteMeat;

	/**
	 * If this field is true, the food can be consumed even if the player don't need to eat.
	 */
	private boolean alwaysEdible;

	/**
	 * represents the potion effect that will occurr upon eating this food. Set by setPotionEffect
	 */
	private int potionId;

	/** set by setPotionEffect */
	private int potionDuration;

	/** set by setPotionEffect */
	private int potionAmplifier;

	/** probably of the set potion effect occurring */
	private float potionEffectProbability;

	/** par1 is ID, par2 is healAmount, par3 is saturation, par4 is isWolfsFavoriteMeat **/
	public ItemTNoodleFood(int par1, int par2, float par3, boolean par4) {
		super(par1, par2, par3, par4);
		this.itemUseDuration = 32;
		this.healAmount = par2;
		this.isWolfsFavoriteMeat = par4;
		this.saturationModifier = par3;
		this.setCreativeTab(TCraftMain.tabTCraft);
	}

	@SideOnly(Side.CLIENT)
	public boolean isFull3D() {
		return false;
	}

	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister par1IconRegister) {
		this.itemIcon = par1IconRegister.registerIcon(ModInfo.modid + ":" + this.getUnlocalizedName().substring(5));
	}

	public ItemTNoodleFood(int par1, int par2, boolean par3) {
		this(par1, par2, 0.6F, par3);
	}

	/**
	 * How long it takes to use or consume an item
	 */
	public int getMaxItemUseDuration(ItemStack par1ItemStack) {
		return 32;
	}

	/**
	 * returns the action that specifies what animation to play when the items is being used
	 */
	public EnumAction getItemUseAction(ItemStack par1ItemStack) {
		return EnumAction.eat;
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
	 */
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
		if (par3EntityPlayer.canEat(this.alwaysEdible)) {
			par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
		}

		return par1ItemStack;
	}

	public int getHealAmount() {
		return this.healAmount;
	}

	/**
	 * gets the saturationModifier of the ItemFood
	 */
	public float getSaturationModifier() {
		return this.saturationModifier;
	}

	/**
	 * Whether wolves like this food (true for raw and cooked porkchop).
	 */
	public boolean isWolfsFavoriteMeat() {
		return this.isWolfsFavoriteMeat;
	}

	/**
	 * sets a potion effect on the item. Args: int potionId, int duration (will be multiplied by 20), int amplifier, float probability of effect happening
	 */
	public ItemTNoodleFood setPotionEffect(int par1, int par2, int par3, float par4) {
		this.potionId = par1;
		this.potionDuration = par2;
		this.potionAmplifier = par3;
		this.potionEffectProbability = par4;
		return this;
	}

	/**
	 * Set the field 'alwaysEdible' to true, and make the food edible even if the player don't need to eat.
	 */
	public ItemTNoodleFood setAlwaysEdible() {
		this.alwaysEdible = true;
		return this;
	}
}

