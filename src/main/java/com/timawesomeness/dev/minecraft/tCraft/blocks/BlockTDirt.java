/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.blocks;

import com.timawesomeness.dev.minecraft.tCraft.ModInfo;
import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockTDirt extends Block {

	public BlockTDirt(int par1, Material blockMaterial) {
		super(par1, blockMaterial);
		setCreativeTab(TCraftMain.tabTCraft);
		setHardness(0.5F);
		setStepSound(soundGravelFootstep);
	}
	
	 @SideOnly(Side.CLIENT) 
     public void registerIcons(IconRegister par1IconRegister)
     {
         this.blockIcon = par1IconRegister.registerIcon(ModInfo.modid + ":" + this.getUnlocalizedName().substring(5));
     }

}
