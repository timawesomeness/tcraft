/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.world;

import net.minecraft.world.WorldProvider;
import net.minecraft.world.biome.WorldChunkManagerHell;
import net.minecraft.world.chunk.IChunkProvider;

import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TDimensionWorldProvider extends WorldProvider {

	@Override
	public String getDimensionName() {
		return "t Dimension";
	}

	public void registerWorldChunkManager() {
		this.worldChunkMgr = new WorldChunkManagerHell(TCraftMain.tBiome, 0.7F, 0.0F);
		this.isHellWorld = false;
		this.dimensionId = TCraftMain.tDimensionID;
	}
	
	public IChunkProvider createChunkGenerator() {
		return new TDimensionChunkProvider(worldObj, worldObj.getSeed(), false);
	}
	
	public double getMovementFactor() {
		return 1.0;
	}
	
	@SideOnly(Side.CLIENT)
	public float getStarBrightness(float f) {
		return 1.0F;
	}
	
	@SideOnly(Side.CLIENT)
	public String getWelcomeMessage() {
		return "Welcome to the t Dimension!";
	}
	
	public long getWorldTime() {
		return 6000L;
	}
	
	@SideOnly(Side.CLIENT)
	public float getCloudHeight() {
		return 500F;
	}

}
