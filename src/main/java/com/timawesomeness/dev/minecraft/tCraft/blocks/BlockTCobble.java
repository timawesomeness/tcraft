/* Copyright (C) 2014 timawesomeness. Use of this source code is governed by the timawesomeness Open Source License v1.4. */ package com.timawesomeness.dev.minecraft.tCraft.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;

import com.timawesomeness.dev.minecraft.tCraft.ModInfo;
import com.timawesomeness.dev.minecraft.tCraft.TCraftMain;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockTCobble extends Block {

	public BlockTCobble(int par1, Material par2Material) {
		super(par1, par2Material);
		setCreativeTab(TCraftMain.tabTCraft);
		setHardness(0.7F);
		setStepSound(soundStoneFootstep);
	}
	
	 @SideOnly(Side.CLIENT) 
     public void registerIcons(IconRegister par1IconRegister)
     {
         this.blockIcon = par1IconRegister.registerIcon(ModInfo.modid + ":" + this.getUnlocalizedName().substring(5));
     }
	 
}
